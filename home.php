<?php 

session_start();

?>


<html>
<head>
	<title> Restaurant home </title>
	<link rel="stylesheet" type="text/css" href="home.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<nav class="navbar navbar-expand-lg">
  <img src="img/logo new.png" alt="logo" class="img-logo">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse" style="text-align:right">
    <div class="navbar-nav ml-auto">
      <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="#categories">Foods</a>
      <a class="nav-item nav-link" href="#social-media">Contact us</a>
	  <?php if (isset($_SESSION['name'])): ?>
	  <a class="nav-item nav-link" href="logout.php">Logout</a>
	  <?php else: ?>
      <a class="nav-item nav-link" href="login&register.php">Login</a>
	  <?php endif ?>
    </div>
  </div>
</nav>
	</div>

	<section class="food-search text-center" style="height:30%">
	<div class="container">
	<form action="search.php" method='GET'>
	<input type="text" name="query" placeholder="search food here...">
	<input type="submit" name="submit" value="search">
	</form>
	</div>
	</section>

	<section class="categories">
	<div class="container" id="categories">
	<h2>Explore Foods</h2>
	<a href="pizza.php">
	<div class="box-3 float-container">
	<img src="img/pizza.jpg" alt="pizza" class="img-responsive img-curve">
	<h3 class="float-text text-white">Pizza</h3>
	</div>
	</a>
	
	<a href="burger.php">
	<div class="box-3 float-container">
	<img src="img/burger.jpg" alt="burger" class="img-responsive img-curve">
	<h3 class="float-text text-white">Burger</h3>
	</div>
	</a>
	
	<a href="rice.php">
	<div class="box-3 float-container">
	<img src="img/rice.jpg" alt="rice" class="img-responsive img-curve">
	<h3 class="float-text text-white">Rice</h3>
	</div>
	</a>
	
	<div class="clearfix"></div>
	</div>
	</section>

	<section class="food-menu">
	<div class="container">
	<h2>Food Menu</h2>
	<div class="food-menu-box float-containere">
		<div class="food-menu-img">
		<img src="img/c-pizza.jpg" alt="chicken pizza" class="img-responsive img-curve">
		</div>
		<div class="food-menu-desc float-texte">
			<h4>Chicken Pizza</h4>
			<p class="food-price">Rs.150.00</p>
			<p class="food-detail">Made with cheese, ittalian sauce and organic vegetables</p>
			<a href="order_form.php" class="btn btn-primary" style="background-color:#Ff69B4">Order now</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="food-menu-box float-containere">
	<div class="food-menu-img">
		<img src="img/s-burger.jpg" alt="smokey burger" class="img-responsive img-curve">
		</div>
		<div class="food-menu-desc float-texte">
			<h4>Smokey Burger</h4>
			<p class="food-price">Rs.150.00</p>
			<p class="food-detail">Made with cheese, ittalian sauce and organic vegetables</p>
			<a href="order_form.php" class="btn btn-primary" style="background-color:#Ff69B4">Order now</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="food-menu-box float-containere">
	<div class="food-menu-img">
		<img src="img/f-rice.jpg" alt="friend rice" class="img-responsive img-curve">
		</div>
		<div class="food-menu-desc float-texte">
			<h4>Fried Rice</h4>
			<p class="food-price">Rs.150.00</p>
			<p class="food-detail">Made with cheese, ittalian sauce and organic vegetables</p>
			<a href="order_form.php" class="btn btn-primary" style="background-color:#Ff69B4">Order now</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="food-menu-box float-containere">
	<div class="food-menu-img">
		<img src="img/briyani.jpg" alt="Briyani" class="img-responsive img-curve">
		</div>
		<div class="food-menu-desc float-texte">
			<h4>Briyani</h4>
			<p class="food-price">Rs.150.00</p>
			<p class="food-detail">Made with cheese, ittalian sauce and organic vegetables</p>
			<a href="order_form.php" class="btn btn-primary" style="background-color:#Ff69B4">Order now</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	</div>
	</section>

	<section class="social-media" id="social-media">
	<div class="container text-center">
	<ul>
	<li> <a href="#"> <img src="https://img.icons8.com/cute-clipart/64/000000/facebook-new.png"/></a> </li>
	<li> <a href="#"> <img src="https://img.icons8.com/cute-clipart/64/000000/instagram-new.png"/> </a> </li>
	<li> <a href="#"> <img src="https://img.icons8.com/cute-clipart/64/000000/twitter.png"/> </a> </li>
	</ul>
	</div>
	</section>

	<section class="footer">
	<div class="container text-center">
	<p class="anchor">All rights reserved. Designed By <a href="a"> Rubini karunakaran</a></p>
	</div>
	</section>
</body>
</html>
