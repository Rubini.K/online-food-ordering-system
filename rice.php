<html>
<head>
	<title> Rice </title>
	<link rel="stylesheet" type="text/css" href="home.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<section class="titlep">
	<div class="container">
		<h2>Rice Menu</h2>
	</div>
</section>
<?php
$con= mysqli_connect('localhost', 'root','');

mysqli_select_db($con, 'e rental system');
 $results = mysqli_query($con, "SELECT * FROM product where product_type='rice'"); ?>

<?php while ($row = mysqli_fetch_array($results)) { ?>

	<div class="food-menu-box float-containere">
		<div class="food-menu-img">
		<img src="img/<?php echo $row['image']; ?>" alt="pizza" class="img-responsive img-curve">
		</div>
		<div class="food-menu-desc float-texte">
			<h4><?php echo $row['name']; ?></h4><br>
			<p class="food-price">Rs.<?php echo $row['price']; ?></p><br>
			<a href="order_form.php" class="btn btn-primary" style="background-color:#Ff69B4">Order now</a>
		</div>
		<div class="clearfix"></div>
	</div>
<?php } ?>