<html>
<head>
	<title> Admin Page </title>
	<link rel="stylesheet" type="text/css" href="admin.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<script>
  function myFunction() {
    window.location.href="add-product-form.php";
  }
 </script>
<section class="title">
<div class="container">
<h2>I'm the BOSS</h2>
<a class="order" href="order_details.php">Order Details</a>
<a class="logout" href="logout.php">Logout</a>
</div>
</section>
<?php include 'editing.php'; ?>
<?php
$con= mysqli_connect('localhost', 'root','');

mysqli_select_db($con, 'e rental system');
 $results = mysqli_query($con, "SELECT * FROM product"); ?>

<table>
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Product Type</th>
			<th>Image</th>
			<th>Price</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	<?php while ($row = mysqli_fetch_array($results)) { ?>

		<tr>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['product_type']; ?></td>
			<td><?php echo $row['image']; ?></td>
			<td><?php echo $row['price']; ?></td>
			<td>
				<a href="add-product-form.php?edit=<?php echo $row['name'];?>" class="edit_btn" >Edit</a>
			</td>
			<td>
				<a href="editing.php?del=<?php echo $row['name']; ?>" class="del_btn">Delete</a>
			</td>
		</tr>
	<?php } ?>

</table>
	
<section class="Add-product">
<div class="container">
<button type="submit" class="btn btn-primary" onClick="myFunction()" style="background-color:#f08080; border-color:black;"> Add Products </button>
</div>
</section>
</body>
</html>