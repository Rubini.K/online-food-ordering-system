<html>
<head>
	<title> Order details Page </title>
	<link rel="stylesheet" type="text/css" href="admin.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<section class="title">
<div class="container">
<h2>Details of Order</h2>
</div>
</section>
<?php include 'editing.php'; ?>
<?php
$con= mysqli_connect('localhost', 'root','');

mysqli_select_db($con, 'e rental system');
 $results = mysqli_query($con, "SELECT * FROM order_product"); ?>

<table>
	<thead>
		<tr>
			<th>Order id</th>
			<th>Customer name</th>
			<th>Location</th>
			<th>Telephone number</th>
			<th>Product name</th>
			<th>Payment</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	<?php while ($row = mysqli_fetch_array($results)) { ?>

		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['location']; ?></td>
			<td><?php echo $row['telephone']; ?></td>
			<td><?php echo $row['food_name']; ?></td>
			<td><?php echo $row['total']; ?></td>
			<td>
				<a href="add-product-form.php?com=<?php echo $row['id'];?>" class="edit_btn" >OrderComplted</a>
			</td>
		</tr>
	<?php } ?>

</table>
</body>
</html>