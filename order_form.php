<?php 

session_start();

if (isset($_SESSION['name'])) {?>

<html>
<head>
	<title>form for order</title>
	<link rel="stylesheet" type="text/css" href="add-product-form.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>

<section class="form">
	<form method="post" action="ordering.php" >
	<h3>Order Product</h3>
		<div class="input-group">
			<label>Name</label>
			<input type="text" name="name" value="<?php echo $_SESSION['name']; ?>" required>
		</div>
		<div class="input-group">
			<label>Location</label>
			<input type="text" name="location" value="" required>
		</div>		
		<div class="input-group">
			<label>Telephone number:</label>
			<input type="text" name="tele" value="" required>
		</div>
		<div class="input-group">
			<label>Food name</label>
			<input type="text" name="food_name" value="<?php echo $_SESSION['product_name']; ?>" required>
		</div>
		<div class="input-group">
			<label>Total amount</label>
			<input type="text" name="total" value="" required>
		</div>
		<div class="input-group">
			<button class="btn" type="submit" name="submit" style="background-color:#5F9EA0; border-color:black;">submit</button>
		</div>
	</form>
	</section>
	
</body>
</html>
<?php 

}
else{

     header("Location: login&register.php");

     exit();

}

 ?>