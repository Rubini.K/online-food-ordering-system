<!DOCTYPE html>
<html>
<head>
	<title>form for product addition</title>
	<link rel="stylesheet" type="text/css" href="add-product-form.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<?php include 'editing.php'; ?>
<section class="form">
	<form method="post" action="adding.php" >
	<h3>Adding Products</h3>
		<div class="input-group">
			<label>Name of the product</label>
			 <?php if (isset($_GET['edit'])): ?>
			 <input type="text" name="name" value="<?php echo $name; ?>">
			 <?php else: ?>
			<input type="text" name="name" value="" required>
			<?php endif ?>
		</div>
		<div class="input-group">
			<label>Food Type</label>
			<select name="product_type" required>  
			<?php if (isset($_GET['edit'])): ?>
			<option value="<?php echo $product_type; ?>"><?php echo $product_type; ?></option> 
			<?php else: ?>
				<option value="Select">Select food type</option>
			<?php endif ?>				
				<option value="pizza">Pizza</option>  
				<option value="burger">Burger</option> 
				<option value="rice">Rice</option>			
			</select> 
		</div>		
		<div class="input-group">
			<label>Select image to upload:</label>
			<?php if (isset($_GET['edit'])): ?>
			<input type="file" name="fileToUpload" id="<?php echo $image; ?>" value="<?php echo $image; ?>" required>
			<?php else: ?>
			<input type="file" name="fileToUpload" id="fileToUpload" required>
			<?php endif ?>
		</div>
		<div class="input-group">
			<label>Price</label>
			<?php if (isset($_GET['edit'])): ?>
			<input type="text" name="price" value="<?php echo $price; ?>" required>
			<?php else: ?>
			<input type="text" name="price" value="" required>
			<?php endif ?>
		</div>
		<div class="input-group">
		<?php if (isset($_GET['edit'])): ?>
	<button class="btn" type="submit" name="update" style="background-color:#5F9EA0; border-color:black;">update</button>
<?php else: ?>
	<button class="btn" type="submit" name="add" style="background-color:#5F9EA0; border-color:black;">add</button>
<?php endif ?>
		</div>
	</form>
	</section>
	
</body>
</html>